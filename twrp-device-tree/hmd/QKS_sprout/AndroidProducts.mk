#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/omni_QKS_sprout.mk

COMMON_LUNCH_CHOICES := \
    omni_QKS_sprout-user \
    omni_QKS_sprout-userdebug \
    omni_QKS_sprout-eng
