#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_QKS_sprout.mk

COMMON_LUNCH_CHOICES := \
    lineage_QKS_sprout-user \
    lineage_QKS_sprout-userdebug \
    lineage_QKS_sprout-eng
